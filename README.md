# Canvas-Path
Path

<div><canvas id="Canvas2" width="600" height = "200" style="border:solid 1px #000000;"></canvas>
   <div>
     <button onclick="Vertical_line();return true;">Click me to draw a brown vertical line</button>
   </div> 
</div>
<script>
   var c3 = document.getElementById("c3");
   var c3_context = c3.getContext("2d");

   function Vertical_line() {
     c3_context.moveTo(300, 10);
     c3_context.lineTo(300, 190);
     c3_context.strokeStyle = "brown";
     c3_context.stroke();
   }
</script>
